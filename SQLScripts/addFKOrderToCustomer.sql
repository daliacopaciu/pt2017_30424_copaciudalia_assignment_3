ALTER TABLE `order`
ADD CONSTRAINT `orderToCustomer`
FOREIGN KEY (`customerID`)
REFERENCES `customer` (`id`)
ON DELETE cascade
ON UPDATE cascade