ALTER TABLE orderdetails 
ADD CONSTRAINT `detailsToOrder`
FOREIGN KEY (`orderID`) 
REFERENCES `order` (`id`)
ON DELETE cascade
ON UPDATE cascade;

ALTER TABLE orderdetails
ADD CONSTRAINT `detailsToProduct`
FOREIGN KEY (`productID`)
REFERENCES `product` (`id`)
ON DELETE cascade
ON UPDATE cascade