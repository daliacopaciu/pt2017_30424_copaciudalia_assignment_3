package model;

import java.util.ArrayList;
import java.util.List;
/**
 * product categories: FOOD, CLEANING, GARDNING, BEAUTY
 * @author Dali
 *
 */
public enum ProductCategories {
	FOOD,
	CLEANING,
	GARDENING,
	BEAUTY;
	
	/**
	 * 
	 * @param inputString
	 * @return the product category transformed from a string
	 */
	public static ProductCategories fromStringToProductCategory(String inputString){
		if("FOOD".equals(inputString)) { return FOOD;}
		if("CLEANING".equals(inputString)) { return CLEANING;}
		if("GARDENING".equals(inputString)) { return GARDENING;}
		if("BEAUTY".equals(inputString)) { return BEAUTY;}
		return null;
	}
	/**
	 * 
	 * @return the categories as an array of all categories
	 */
	public static List<String> categoriesArray(){
		List<String> categories = new ArrayList<String>();
		categories.add(ProductCategories.FOOD.toString());
		categories.add(ProductCategories.BEAUTY.toString());
		categories.add(ProductCategories.GARDENING.toString());
		categories.add(ProductCategories.CLEANING.toString());
		return categories;
	}
}
