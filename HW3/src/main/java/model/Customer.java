package model;
/**
 * 
 * customer table with fields: id( int), name( String), budget( int), address( String), email( String)
 *
 */
public class Customer {
   private int id;
   private String name;
   private int budget;
   private String address;
   private String email;
   /**
    * Constructor with
    * @param id
    * @param name
    * @param budget
    * @param address
    * @param email
    */
public Customer(int id, String name,int budget, String address, String email) {
	super();
	this.id = id;
	this.name = name;
	this.budget=budget;
	this.address = address;
	this.email = email;
}
/**
 * Constructor with 
 * @param name
 * @param budget
 * @param address
 * @param email
 */
public Customer( String name,int budget, String address, String email) {
	super();
	this.name = name;
	this.budget=budget;
	this.address = address;
	this.email = email;
}
public Customer(){
	
}
public int getId() {
	return id;
}
public int getBudget() {
	return budget;
}
public void setBudget(int budget) {
	this.budget = budget;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
   
}
