package model;
/**
 * 
 * represents orderdetails table
 *
 */
public class OrderDetails {
	private int id;
	private int orderID;
	private int productID;
	private int quantity;
	/**
	 * Constructor with 
	 * @param id
	 * @param orderID
	 * @param productID
	 * @param quantity
	 */
	public OrderDetails(int id, int orderID, int productID, int quantity) {
		super();
		this.id = id;
		this.orderID = orderID;
		this.productID = productID;
		this.quantity = quantity;
	}
	/**
	 * Constructor with
	 * @param orderID
	 * @param productID
	 * @param quantity
	 */
	public OrderDetails(int orderID, int productID, int quantity) {
		super();
		this.orderID = orderID;
		this.productID = productID;
		this.quantity = quantity;
	}
	public OrderDetails(){}
	public int getOrderID() {
		return orderID;
	}
	public int getProductID() {
		return productID;
	}
	public void setProductID(int productID) {
		this.productID = productID;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getId() {
		return id;
	}
	public void setOrderID(int val){
		orderID=val;
	}
}
