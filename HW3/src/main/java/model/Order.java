package model;
/**
 * 
 * @author Dali
 * 
 *
 */
public class Order {
	private int id;
	private int customerID;
	private int totalPrice;
	/**
	 * Constructor with 
	 * @param id
	 * @param customerID
	 * @param totalPrice
	 */
	public Order(int id, int customerID,int totalPrice) {
		super();
		this.id = id;
		this.customerID = customerID;
		this.totalPrice=totalPrice;
	} 
	/**
	 * Constructor with parameter: 
	 * @param customerID
	 */
	public Order(int customerID) {
		super();
		this.customerID = customerID;
	} 
	public Order() {
	}
	public int getId() {
		return id;
	}
	public int getCustomerID() {
		return customerID;
	}
	public void setCustomerID(int id){
		customerID=id;
	}
	public int getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}
	
}
