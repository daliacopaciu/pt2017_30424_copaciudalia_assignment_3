package model;
/**
 * 
 * @author Dali
 * @represents the product table
 */
public class Product {
	private int id;
	private String name;
	private int price;
	private ProductCategories category;
	private int weight;
	private int stock;
	/**
	 * Constructor with
	 * @param id
	 * @param name
	 * @param price
	 * @param category
	 * @param weight
	 * @param stock
	 */
	public Product(int id, String name, int price, ProductCategories category,int weight,int stock){
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.category = category;
		this.weight=weight;
		this.stock=stock;
	}
	/**
	 * Constructor with
	 * @param name
	 * @param price
	 * @param category
	 * @param weight
	 * @param stock
	 */
	public Product(String name, int price, ProductCategories category, int weight, int stock) {
		super();
		this.name = name;
		this.price = price;
		this.category = category;
		this.weight = weight;
		this.stock = stock;
	} 

	public Product(){
		
	}
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public ProductCategories getCategory() {
		return category;
	}
	public void setCategory(ProductCategories category) {
		this.category = category;
	}
	public int getWeight(){
		return weight;
	}
	public void setWeight(int weight){
	    this.weight=weight;
	}
	public int getStock(){
		return stock;
	}
	public void setStock(int stock){
	    this.stock=stock;
	}
	
}
