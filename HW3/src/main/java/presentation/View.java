package presentation;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.util.List;
import javax.swing.JTable;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JComboBox;

public class View {

	public JFrame frame;
	private JScrollPane scrollForProducts;
	private JButton btnShowProducts;
	private JTable table;
	private JTabbedPane tabbedPane;
	private JPanel panel;
	private JTable table_customer;
	private JButton btnShowCustomers;
	private JTextField productName;
	private JTextField productprice;
	private JTextField productWeight;
	private JTextField productStock;
	private JComboBox<String> productCategory;
	private JComboBox<String> customersToDelete;
	private JComboBox<String> fieldsToEdit;
	private JComboBox<String> ordDetToDelete;
	private JButton btnAddProduct;
	private JComboBox<String> productsToDelete;
	private JButton btnDeleteProduct;
	private JButton btnAddCustomer;
	private JButton btnDeleteCustomer;
	private JButton btnEditProduct;
	private JTextField cName;
	private JTextField cBudget;
	private JTextField cEmail;
	private JLabel lblNewLabel_1;
	private JLabel lblBudget;
	private JLabel lblAddress;
	private JLabel lblEmail;
	private JTextField cAddres;
	private JTable table_order;
	private JTextField orderQuantity;
	private JTextField newEditedValue;
	private JLabel lblNewLabel_2;
	private JComboBox<String> orderCustomer;
	private JComboBox<String> orderProduct;
	private JButton btnAddOrder;
	private JButton btnShowOrders;
	private JButton btnRemoveOrder;
	private JComboBox<String> orderToDelete;
    private JPanel panel2;
    private JPanel panel3;
    private JButton btnFindByCategory;
    private JButton btnBills;
    private JComboBox<String> findCategory;
    private JButton btnFindByPrice;
    private JLabel label;
    private JTextField priceLessThan;
    private JButton btnEditCustomer;
    private JComboBox<String> fieldsCtoEdit;
    private JTextField newCvalue;
    private JLabel lblNewValue;
    private JButton btnEditOrder;
    private JComboBox<String> orderFields;
    private JTextField orderValue;
    private JLabel lblNewValue_1;
    private JScrollPane scrollPane_2;
    private JTable table_ord_det;
    private JButton btnAddOrderDetail;
    private JButton btnRemoveOrderDetail;
    private JButton btnEditOrdDet;
    private JTextField newOrdDetVal;
    private JLabel label_1;

	public View(List<String> productCategories) {
		initialize(productCategories);

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(List<String> productCategories) {
		frame = new JFrame();
		frame.setBounds(100, 100, 750, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 23, 650, 499);
		frame.getContentPane().add(tabbedPane);

		panel = new JPanel();
		tabbedPane.addTab("Product", null, panel, null);
		panel.setLayout(null);

		table = new JTable();
		table.setBounds(222, 135, 376, 234);
		panel.add(table);
		
		scrollForProducts = new JScrollPane(table);
		scrollForProducts.setBounds(222, 135, 376, 234);
		panel.add(scrollForProducts);

		btnShowProducts = new JButton("Show products");
		btnShowProducts.setBounds(10, 11, 106, 23);
		panel.add(btnShowProducts);

		btnAddProduct = new JButton("Add Product");

		btnAddProduct.setBounds(10, 48, 106, 23);
		panel.add(btnAddProduct);
		productName = new JTextField();
		productName.setBounds(126, 49, 86, 20);
		panel.add(productName);
		productName.setColumns(10);

		productprice = new JTextField();
		productprice.setColumns(10);
		productprice.setBounds(222, 49, 86, 20);
		panel.add(productprice);

		productWeight = new JTextField();
		productWeight.setColumns(10);
		productWeight.setBounds(414, 49, 86, 20);
		panel.add(productWeight);

		productStock = new JTextField();
		productStock.setColumns(10);
		productStock.setBounds(512, 49, 86, 20);
		panel.add(productStock);

		JLabel lblNewLabel = new JLabel("Name");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel.setBounds(143, 34, 46, 14);
		panel.add(lblNewLabel);

		JLabel lblPrice = new JLabel("Price");
		lblPrice.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblPrice.setBounds(222, 35, 46, 14);
		panel.add(lblPrice);

		JLabel lblCategory = new JLabel("Category");
		lblCategory.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblCategory.setBounds(323, 35, 59, 14);
		panel.add(lblCategory);

		JLabel lblWeight = new JLabel("Weight");
		lblWeight.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblWeight.setBounds(414, 35, 46, 14);
		panel.add(lblWeight);

		JLabel lblStock = new JLabel("Stock");
		lblStock.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblStock.setBounds(515, 35, 46, 14);
		panel.add(lblStock);

		productCategory = new JComboBox(productCategories.toArray());
		productCategory.setBounds(318, 49, 86, 20);
		panel.add(productCategory);
		
		productsToDelete = new JComboBox<String>();
		productsToDelete.setBounds(126, 104, 106, 20);
		panel.add(productsToDelete);
		
		btnDeleteProduct = new JButton("Delete product");
		btnDeleteProduct.setBounds(10, 89, 106, 23);
		panel.add(btnDeleteProduct);
		
		btnEditProduct = new JButton("Edit product");
		btnEditProduct.setBounds(10, 132, 106, 23);
		panel.add(btnEditProduct);
		
		fieldsToEdit = new JComboBox<String>();
		fieldsToEdit.setBounds(126, 133, 86, 20);
		panel.add(fieldsToEdit);
		
		newEditedValue = new JTextField();
		newEditedValue.setBounds(103, 174, 86, 20);
		panel.add(newEditedValue);
		newEditedValue.setColumns(10);
		
		lblNewLabel_2 = new JLabel("New value");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_2.setBounds(21, 177, 72, 14);
		panel.add(lblNewLabel_2);
		
		btnFindByCategory = new JButton("Find by category");
		btnFindByCategory.setBounds(10, 234, 136, 23);
		panel.add(btnFindByCategory);
		
		findCategory = new JComboBox(productCategories.toArray());
		findCategory.setBounds(153, 235, 72, 20);
		panel.add(findCategory);
		
		btnFindByPrice = new JButton("Find by price");
		btnFindByPrice.setBounds(10, 273, 106, 23);
		panel.add(btnFindByPrice);
		
		label = new JLabel("<=");
		label.setBounds(126, 277, 46, 14);
		panel.add(label);
		
		priceLessThan = new JTextField();
		priceLessThan.setBounds(143, 274, 46, 20);
		panel.add(priceLessThan);
		priceLessThan.setColumns(10);

		panel2 = new JPanel();
		tabbedPane.addTab("Customer", null, panel2, null);
		panel2.setLayout(null);

		btnShowCustomers = new JButton("Show customers");
		btnShowCustomers.setBounds(10, 11, 111, 28);
		panel2.add(btnShowCustomers);
		
		btnAddCustomer = new JButton("Add Customer");
		btnAddCustomer.setBounds(10, 49, 111, 23);
		panel2.add(btnAddCustomer);
		
		cName = new JTextField();
		cName.setBounds(147, 50, 86, 20);
		panel2.add(cName);
		cName.setColumns(10);
		
		cBudget = new JTextField();
		cBudget.setColumns(10);
		cBudget.setBounds(243, 50, 86, 20);
		panel2.add(cBudget);
		
		
		cEmail = new JTextField();
		cEmail.setColumns(10);
		cEmail.setBounds(433, 50, 86, 20);
		panel2.add(cEmail);
		
		lblNewLabel_1 = new JLabel("Name");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel_1.setBounds(162, 36, 46, 14);
		panel2.add(lblNewLabel_1);
		
		lblBudget = new JLabel("Budget");
		lblBudget.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblBudget.setBounds(256, 37, 46, 14);
		panel2.add(lblBudget);
		
		lblAddress = new JLabel("Address");
		lblAddress.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblAddress.setBounds(352, 37, 46, 14);
		panel2.add(lblAddress);
		
		lblEmail = new JLabel("Email");
		lblEmail.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblEmail.setBounds(445, 37, 46, 14);
		panel2.add(lblEmail);
		
		cAddres = new JTextField();
		cAddres.setBounds(337, 50, 86, 20);
		panel2.add(cAddres);
		cAddres.setColumns(10);
		
		btnDeleteCustomer = new JButton("Delete Customer");
		btnDeleteCustomer.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnDeleteCustomer.setBounds(10, 83, 123, 23);
		panel2.add(btnDeleteCustomer);
		
		customersToDelete = new JComboBox<String>();
		customersToDelete.setBounds(133, 97, 86, 20);
		panel2.add(customersToDelete);
		
		btnEditCustomer = new JButton("Edit Customer");
		btnEditCustomer.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnEditCustomer.setBounds(10, 124, 111, 23);
		panel2.add(btnEditCustomer);
		
		fieldsCtoEdit = new JComboBox<String>();
		fieldsCtoEdit.setBounds(133, 125, 75, 20);
		panel2.add(fieldsCtoEdit);
		
		newCvalue = new JTextField();
		newCvalue.setBounds(103, 158, 86, 20);
		panel2.add(newCvalue);
		newCvalue.setColumns(10);
		
		lblNewValue = new JLabel("New Value");
		lblNewValue.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewValue.setBounds(10, 164, 77, 14);
		panel2.add(lblNewValue);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(192, 158, 422, 253);
		panel2.add(scrollPane);
		
				table_customer = new JTable();
				scrollPane.setViewportView(table_customer);
		
		panel3 = new JPanel();
		tabbedPane.addTab("Order", null, panel3, null);
		panel3.setLayout(null);
		
		btnShowOrders = new JButton("Show Orders");
		btnShowOrders.setBounds(204, 195, 117, 23);
		panel3.add(btnShowOrders);
		
		btnAddOrder = new JButton("Add Order");
		btnAddOrder.setBounds(24, 257, 115, 23);
		panel3.add(btnAddOrder);
		
		orderCustomer = new JComboBox<String>();
		orderCustomer.setBounds(154, 258, 101, 20);
		panel3.add(orderCustomer);
		
		orderProduct = new JComboBox<String>();
		orderProduct.setBounds(278, 310, 101, 20);
		panel3.add(orderProduct);
		
		orderQuantity = new JTextField();
		orderQuantity.setBounds(389, 310, 86, 20);
		panel3.add(orderQuantity);
		orderQuantity.setColumns(10);
		
		JLabel lblCustomer = new JLabel("Customer");
		lblCustomer.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblCustomer.setBounds(169, 242, 66, 14);
		panel3.add(lblCustomer);
		
		JLabel lblProduct = new JLabel("Product");
		lblProduct.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblProduct.setBounds(297, 297, 66, 14);
		panel3.add(lblProduct);
		
		JLabel lblQuantity = new JLabel("Quantity");
		lblQuantity.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblQuantity.setBounds(403, 297, 66, 14);
		panel3.add(lblQuantity);
		
		btnRemoveOrder = new JButton("Remove Order");
		btnRemoveOrder.setBounds(24, 309, 115, 23);
		panel3.add(btnRemoveOrder);
		
		orderToDelete = new JComboBox<String>();
		orderToDelete.setBounds(154, 310, 101, 20);
		panel3.add(orderToDelete);
		
		btnEditOrder = new JButton("Edit Customer ID");
		btnEditOrder.setBounds(24, 372, 115, 23);
		panel3.add(btnEditOrder);
		
		orderFields = new JComboBox<String>();
		orderFields.setBounds(389, 415, 86, 20);
		panel3.add(orderFields);
		
		orderValue = new JTextField();
		orderValue.setBounds(154, 373, 86, 20);
		panel3.add(orderValue);
		orderValue.setColumns(10);
		
		lblNewValue_1 = new JLabel("New value");
		lblNewValue_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewValue_1.setBounds(169, 393, 66, 14);
		panel3.add(lblNewValue_1);
		
		btnBills = new JButton("Create Bills");
		btnBills.setBounds(348, 195, 116, 23);
		panel3.add(btnBills);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 24, 258, 158);
		panel3.add(scrollPane_1);
		
		table_order = new JTable();
		scrollPane_1.setViewportView(table_order);
		
		scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(278, 24, 330, 158);
		panel3.add(scrollPane_2);
		
		table_ord_det = new JTable();
		scrollPane_2.setViewportView(table_ord_det);
		
		btnAddOrderDetail = new JButton("Add Order Detail");
		btnAddOrderDetail.setBounds(485, 309, 150, 23);
		panel3.add(btnAddOrderDetail);
		
		JLabel lblOrders = new JLabel("Orders");
		lblOrders.setBounds(186, 298, 46, 14);
		panel3.add(lblOrders);
		
		JLabel lblFieldsToEdit = new JLabel("Fields to Edit");
		lblFieldsToEdit.setBounds(389, 446, 69, 14);
		panel3.add(lblFieldsToEdit);
		
		JLabel lblOrders_1 = new JLabel("Orders");
		lblOrders_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblOrders_1.setBounds(90, 3, 66, 14);
		panel3.add(lblOrders_1);
		
		JLabel lblOrderDetails = new JLabel("Order Details");
		lblOrderDetails.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblOrderDetails.setBounds(430, 5, 107, 14);
		panel3.add(lblOrderDetails);
		
		btnRemoveOrderDetail = new JButton("Remove Order Detail");
		btnRemoveOrderDetail.setBounds(485, 361, 150, 23);
		panel3.add(btnRemoveOrderDetail);
		
		ordDetToDelete = new JComboBox<String>();
		ordDetToDelete.setBounds(389, 375, 86, 20);
		panel3.add(ordDetToDelete);
		
		JLabel lblOrderDetails_1 = new JLabel("Order Details");
		lblOrderDetails_1.setBounds(389, 361, 86, 14);
		panel3.add(lblOrderDetails_1);
		
		btnEditOrdDet = new JButton("Edit Order Details");
		btnEditOrdDet.setBounds(485, 403, 150, 23);
		panel3.add(btnEditOrdDet);
		
		newOrdDetVal = new JTextField();
		newOrdDetVal.setBounds(293, 415, 86, 20);
		panel3.add(newOrdDetVal);
		newOrdDetVal.setColumns(10);
		
		label_1 = new JLabel("New value");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		label_1.setBounds(297, 435, 66, 14);
		panel3.add(label_1);
		
	}
	

	public JComboBox<String> getCustomersToDelete() {
		return customersToDelete;
	}

	public JTextField getcAddres() {
		return cAddres;
	}

	public JTextField getcName() {
		return cName;
	}

	public JTextField getcBudget() {
		return cBudget;
	}

	public JTextField getcEmail() {
		return cEmail;
	}

	public JButton getBtnNewButton() {
		return btnShowProducts;
	}
    
	public void err(String msg, String title) {
		JOptionPane.showMessageDialog(null, msg, title, JOptionPane.INFORMATION_MESSAGE);
	}

	public JButton getBtnShowCustomers() {
		return btnShowCustomers;
	}

	public JButton getBtnAddProduct() {
		return btnAddProduct;
	}

	public JTextField getProductName() {
		return productName;
	}

	public JTextField getProductprice() {
		return productprice;
	}

	public JTextField getProductWeight() {
		return productWeight;
	}

	public JTextField getProductStock() {
		return productStock;
	}

	public JComboBox<String> getProductCategory() {
		return productCategory;
	}
	
	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public JTable getTable_customer() {
		return table_customer;
	}

	public void setTable_customer(JTable table_customer) {
		this.table_customer = table_customer;
	}

	public JComboBox<String> getProductsToDelete() {
		return productsToDelete;
	}

	public JButton getBtnDeleteProduct() {
		return btnDeleteProduct;
	}
	
	public JButton getBtnAddCustomer() {
		return btnAddCustomer;
	}

	public JButton getBtnDeleteCustomer() {
		return btnDeleteCustomer;
	}

	public JComboBox<String> getFieldsToEdit() {
		return fieldsToEdit;
	}

	public JButton getBtnEditProduct() {
		return btnEditProduct;
	}

	public JTextField getNewEditedValue() {
		return newEditedValue;
	}

	public JComboBox<String> getOrderCustomer() {
		return orderCustomer;
	}

	public JComboBox<String> getOrderProduct() {
		return orderProduct;
	}

	public JButton getBtnAddOrder() {
		return btnAddOrder;
	}

	public JTextField getOrderQuantity() {
		return orderQuantity;
	}

	public JButton getBtnShowOrders() {
		return btnShowOrders;
	}

	public JTable getTable_order() {
		return table_order;
	}

	public JButton getBtnRemoveOrder() {
		return btnRemoveOrder;
	}

	public JComboBox<String> getOrderToDelete() {
		return orderToDelete;
	}

	public JButton getBtnFindByCategory() {
		return btnFindByCategory;
	}

	public JComboBox<String> getFindCategory() {
		return findCategory;
	}

	public JButton getBtnFindByPrice() {
		return btnFindByPrice;
	}

	public JTextField getPriceLessThan() {
		return priceLessThan;
	}

	public JButton getBtnEditCustomer() {
		return btnEditCustomer;
	}

	public JComboBox<String> getFieldsCtoEdit() {
		return fieldsCtoEdit;
	}

	public JTextField getnewCvalue() {
		return newCvalue;
	}

	public JButton getBtnEditOrder() {
		return btnEditOrder;
	}

	public JComboBox<String> getOrderFields() {
		return orderFields;
	}

	public JTextField getOrderValue() {
		return orderValue;
	}

	public JButton getBtnBills() {
		return btnBills;
	}

	public JTable getTable_ord_det() {
		return table_ord_det;
	}

	public JButton getBtnAddOrderDetail() {
		return btnAddOrderDetail;
	}

	public JComboBox<String> getOrdDetToDelete() {
		return ordDetToDelete;
	}

	public JButton getBtnRemoveOrderDetail() {
		return btnRemoveOrderDetail;
	}

	public JButton getBtnEditOrdDet() {
		return btnEditOrdDet;
	}

	public JTextField getNewOrdDetVal() {
		return newOrdDetVal;
	}
	
	
	
}