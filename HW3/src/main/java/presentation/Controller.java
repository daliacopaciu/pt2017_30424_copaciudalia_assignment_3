package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;

import businessLogic.ClientAdministration;
import businessLogic.OrderProcessing;
import businessLogic.WarehouseAdministration;
import businessLogic.OrderDetailsProcessing;
import model.Customer;
import model.Order;
import model.OrderDetails;
import model.Product;
import model.ProductCategories;
import validators.CustomerValidator;
import validators.ProductValidator;
import validators.OrderDetailValidator;
/**
 * controls the view and actions on it
 * @author Dali
 *
 */
public class Controller {
	
	private WarehouseAdministration warehouseAdministration;
	private ClientAdministration clientAdministration;
	private CustomerValidator customerValidator;
	private ProductValidator productValidator;
	private OrderProcessing orderProcessing;
	private OrderDetailsProcessing odp;
	private OrderDetailValidator orderDetVal;
	private View view;
	/**
	 * instantiate validators, businessLogic objects,buttons, and view
	 * @param view
	 */
	public Controller ( View view){
		this.warehouseAdministration = new WarehouseAdministration();
		this.clientAdministration = new ClientAdministration();
		this.productValidator = new ProductValidator();
		this.customerValidator = new CustomerValidator();
		this.orderProcessing = new OrderProcessing();
		this.orderDetVal = new OrderDetailValidator();
		this.odp=new OrderDetailsProcessing();
		this.view = view;
		showAllTheProducts();
		showAllCustomers();
		showAllTheOrders();
		addProduct();
		addCustomer();
		addOrder();
		addOrderDetail();
		deleteProduct();
		deleteCustomer();
		deleteOrder();
		deleteOrderDet();
		editProduct();
		editCustomer();
		editOrder();
		findProductByCategory();
		findProductByPrice();
		updateCombo();
		updateFieldsCombo();
		editOrderDetail();
		createBills();
	}
	
	private void showAllTheProducts(){
		view.getBtnNewButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				view.getTable().setModel(warehouseAdministration.showProductBy("all", "0"));
				
			}
		});
	}
	private void showAllTheOrders(){
		view.getBtnShowOrders().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				view.getTable_order().setModel(orderProcessing.fillOrder());
				view.getTable_ord_det().setModel(odp.fillOrderDetail());
			}
		});
	}
	private void showAllCustomers(){
		view.getBtnShowCustomers().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				view.getTable_customer().setModel(clientAdministration.fillCustomer());
			}
		});
	}
	
	private void addProduct(){
		view.getBtnAddProduct().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
				    String name=view.getProductName().getText();
				    int price=Integer.parseInt(view.getProductprice().getText());
				    ProductCategories category = ProductCategories.fromStringToProductCategory(view.getProductCategory().getSelectedItem().toString());
				    int weight = Integer.parseInt(view.getProductWeight().getText());
				    int stock = Integer.parseInt(view.getProductStock().getText());
				    Product product = new Product(name, price, category, weight, stock);
				    productValidator.validate(product);
					warehouseAdministration.insertProduct(product);
				    view.getTable().setModel(warehouseAdministration.showProductBy("all","0"));  //if "all" string is given as parameter, the table has all the existing customers
				    updateCombo();
				}
				catch(Exception e1){
					view.err(e1.getLocalizedMessage(),"Wrong input");
				}
			}
		});
	}
	
	private void addOrder(){
		view.getBtnAddOrder().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
				    String id = view.getOrderCustomer().getSelectedItem().toString().split("\\s+")[0]; 
				    Customer customer=clientAdministration.findById(Integer.parseInt(id));
				    
				    Order order = new Order(customer.getId());
				    orderProcessing.insertOrder(order);
				    view.getTable_order().setModel(orderProcessing.fillOrder());
				    view.getTable().setModel(warehouseAdministration.showProductBy("all","0"));
				    view.getTable_customer().setModel(clientAdministration.fillCustomer());
				    updateCombo();
				}
				catch(Exception e1){
					view.err(e1.getLocalizedMessage(),"Wrong input");
				}
			}
		});
	}
	private void addOrderDetail(){
		view.getBtnAddOrderDetail().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					String ord = view.getOrderToDelete().getSelectedItem().toString().split("\\s+")[0];  //chosen order detail id
					int orderID=Integer.parseInt(ord);
					String selProduct = view.getOrderProduct().getSelectedItem().toString().split("\\s+")[0];
					int productID=Integer.parseInt(selProduct);
				    int quantity = Integer.parseInt(view.getOrderQuantity().getText());
				    
				    OrderDetails order = new OrderDetails(orderID,productID,quantity);
				    orderDetVal.validate(order);
				    odp.insert(order);
				    view.getTable_order().setModel(orderProcessing.fillOrder());
				    view.getTable().setModel(warehouseAdministration.showProductBy("all","0"));
				    view.getTable_customer().setModel(clientAdministration.fillCustomer());
				    view.getTable_ord_det().setModel(odp.fillOrderDetail());
				    updateCombo();
				}
				catch(Exception e1){
					view.err(e1.getLocalizedMessage(),"Wrong input");
				}
			}
		});
	} 
	
	private void addCustomer(){
		view.getBtnAddCustomer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
				    String name=view.getcName().getText();
				    int b=Integer.parseInt(view.getcBudget().getText());
				    String address=view.getcAddres().getText();
				    String email=view.getcEmail().getText();
				    Customer customer = new Customer(name, b, address, email);
				    customerValidator.validate(customer);
				    clientAdministration.insertCustomer(customer);
				    view.getTable_customer().setModel(clientAdministration.fillCustomer());
				    updateCombo();
				}
				catch(Exception e2){
					view.err(e2.getLocalizedMessage(),"Wrong input");
				}
			}
		});
	}
	
	private void deleteProduct(){
		view.getBtnDeleteProduct().addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				try{
					String selectedDeletedItem = view.getProductsToDelete().getSelectedItem().toString();
					String[] splited = selectedDeletedItem.split("\\s+");
					warehouseAdministration.deleteProduct(Integer.parseInt(splited[0]));
					view.getTable().setModel(warehouseAdministration.showProductBy("all", "0"));
				    updateCombo();
				}catch(Exception e2){
					view.err("Smth is wrong!","Wrong input");
				}
			}
		});
	}
	private void deleteCustomer(){
    		view.getBtnDeleteCustomer().addActionListener(new ActionListener() {
    		   public void actionPerformed(ActionEvent e) {
    				try{
    					String selectedDeletedItem = view.getCustomersToDelete().getSelectedItem().toString();
    					String[] splited = selectedDeletedItem.split("\\s+");
    					clientAdministration.deleteCustomer(Integer.parseInt(splited[0]));  //EDIT
    					view.getTable_customer().setModel(clientAdministration.fillCustomer());
    					 updateCombo();
    				}catch(Exception e2){
    					view.err("Smth is wrong!","Wrong input");
    				}
    			}
    		});

    }
	private void deleteOrder(){
    	view.getBtnRemoveOrder().addActionListener(new ActionListener() {
 		   public void actionPerformed(ActionEvent e) {
 				try{
 					String ordDel = view.getOrderToDelete().getSelectedItem().toString();
 					String[] splited = ordDel.split("\\s+");
 					orderProcessing.deleteOrder((Integer.parseInt(splited[0]))); 
 					odp.delete(Integer.parseInt(splited[0]),true);
 					view.getTable_order().setModel(orderProcessing.fillOrder());
 					view.getTable_ord_det().setModel(odp.fillOrderDetail());
 					 updateCombo();
 				}catch(Exception e2){
 					view.err("Smth is wrong!","Wrong input");
 				}
 			}
 		});
    }
	private void deleteOrderDet(){
    	view.getBtnRemoveOrderDetail().addActionListener(new ActionListener() {
 		   public void actionPerformed(ActionEvent e) {
 				try{
 					String ordDel = view.getOrdDetToDelete().getSelectedItem().toString(); 
 					odp.delete(Integer.parseInt(ordDel),false);
 					view.getTable_ord_det().setModel(odp.fillOrderDetail());
 					 updateCombo();
 				}catch(Exception e2){
 					view.err("Smth is wrong!","Wrong input");
 				}
 			}
 		});
    }
    private void editProduct(){
    	view.getBtnEditProduct().addActionListener(new ActionListener(){
    		public void actionPerformed(ActionEvent e) {
    			try{
					String productField = view.getFieldsToEdit().getSelectedItem().toString();
					String selectedProduct = view.getProductsToDelete().getSelectedItem().toString().split("\\s+")[0];
					String value = view.getNewEditedValue().getText();
					Product editedProduct = warehouseAdministration.obtainedProduct(Integer.parseInt(selectedProduct), productField, value);
					warehouseAdministration.editProduct(Integer.parseInt(selectedProduct), productField, value);
					productValidator.validate(editedProduct);
					view.getTable().setModel(warehouseAdministration.showProductBy("all", "0"));
					 updateCombo();
				}catch(Exception e2){
					view.err(e2.getLocalizedMessage(),"Wrong input");
				}
    		} 
    	});
    }
    
    private void findProductByCategory(){
    	view.getBtnFindByCategory().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
				   String category = view.getFindCategory().getSelectedItem().toString();
				    view.getTable().setModel(warehouseAdministration.showProductBy("category",category));
				    
				}
				catch(Exception e1){
					view.err(e1.getLocalizedMessage(),"Wrong input");
				}
			}
		});
    }
    private void findProductByPrice(){
    	view.getBtnFindByPrice().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
				    view.getTable().setModel(warehouseAdministration.showProductBy("price",view.getPriceLessThan().getText()));
				    
				}
				catch(Exception e1){
					view.err(e1.getLocalizedMessage(),"Wrong input");
				}
			}
		});
    }
    
    private void editCustomer(){
    	view.getBtnEditCustomer().addActionListener(new ActionListener(){
    		public void actionPerformed(ActionEvent e) {
    			try{
					String productField = view.getFieldsCtoEdit().getSelectedItem().toString();
					String selectedCustomer = view.getCustomersToDelete().getSelectedItem().toString().split("\\s+")[0];
					String value = view.getnewCvalue().getText();
					Customer editedCustomer = clientAdministration.obtainedCustomer(Integer.parseInt(selectedCustomer), productField, value);
					customerValidator.validate(editedCustomer);
					clientAdministration.edit(Integer.parseInt(selectedCustomer), productField, value);
					view.getTable_customer().setModel(clientAdministration.fillCustomer());
					 updateCombo();
				}catch(Exception e2){
					view.err(e2.getLocalizedMessage(),"Wrong input");
				}
    		} 
    	});
    }
    private void editOrder(){
    	view.getBtnEditOrder().addActionListener(new ActionListener(){
    		public void actionPerformed(ActionEvent e) {
    			try{
					String selectedOrder = view.getOrderToDelete().getSelectedItem().toString().split("\\s+")[0];
					String value = view.getOrderValue().getText();
					orderProcessing.edit(Integer.parseInt(selectedOrder),"customerID", value);
					view.getTable_order().setModel(orderProcessing.fillOrder());
					 updateCombo();
				}catch(Exception e2){
					view.err(e2.getLocalizedMessage(),"Wrong input");
				}
    		} 
    	});
    }
    private void editOrderDetail(){
    	view.getBtnEditOrdDet().addActionListener(new ActionListener(){
    		public void actionPerformed(ActionEvent e) {
    			try{
					String orderField = view.getOrderFields().getSelectedItem().toString();
					String selectedOrder = view.getOrdDetToDelete().getSelectedItem().toString();
					String value = view.getNewOrdDetVal().getText();
					OrderDetails editedOrder=odp.findBy("id", Integer.parseInt(selectedOrder));
					 editedOrder = odp.obtainedOrderDet(Integer.parseInt(selectedOrder), orderField, value);
					orderDetVal.validate(editedOrder);
					Product product=warehouseAdministration.getProductById(editedOrder.getProductID());
					if(Integer.parseInt(value)>product.getStock())
						throw new IllegalArgumentException("The quantity must be less than the stock!");
					odp.edit(Integer.parseInt(selectedOrder),orderField, value);
					view.getTable_ord_det().setModel(odp.fillOrderDetail());
					 updateCombo();
				}catch(Exception e2){
					view.err(e2.getLocalizedMessage(),"Wrong input");
				}
    		} 
    	});
    }
    private void updateCombo() {
		
		DefaultComboBoxModel<String> model = (DefaultComboBoxModel<String>) view.getProductsToDelete().getModel();
		DefaultComboBoxModel<String> model2 = (DefaultComboBoxModel<String>) view.getCustomersToDelete().getModel();
		DefaultComboBoxModel<String> model3 = (DefaultComboBoxModel<String>) view.getOrderToDelete().getModel();
		DefaultComboBoxModel<String> model4 = (DefaultComboBoxModel<String>) view.getOrderCustomer().getModel();
		DefaultComboBoxModel<String> model5 = (DefaultComboBoxModel<String>) view.getOrderProduct().getModel();
		DefaultComboBoxModel<String> model6 = (DefaultComboBoxModel<String>) view.getOrdDetToDelete().getModel();
		model.removeAllElements();
		model2.removeAllElements();
		model3.removeAllElements();
		model4.removeAllElements();
		model5.removeAllElements();
		model6.removeAllElements();
		for (String item : warehouseAdministration.getProductIdsAndNames()) {
		    model.addElement(item);
		    model5.addElement(item);
		}
		for (String item : clientAdministration.getCustomersIdsAndNames()) {
		    model2.addElement(item);
		    model4.addElement(item);
		}
		for (String item : orderProcessing.getCustomersIdsAndNames()) {
		    model3.addElement(item);
		}
		for (String item : odp.getAllOrderDet()) {
		    model6.addElement(item);
		} 
		view.getProductsToDelete().setModel(model);
		view.getCustomersToDelete().setModel(model2);
		view.getOrderToDelete().setModel(model3);
	    view.getOrderCustomer().setModel(model4);
	    view.getOrderProduct().setModel(model5);
	    view.getOrdDetToDelete().setModel(model6);
	}
    private void updateFieldsCombo(){
    	DefaultComboBoxModel<String> model = (DefaultComboBoxModel<String>) view.getFieldsToEdit().getModel();
		model.removeAllElements();
		DefaultComboBoxModel<String> model2 = (DefaultComboBoxModel<String>) view.getFieldsCtoEdit().getModel();
		model2.removeAllElements();
		DefaultComboBoxModel<String> model3 = (DefaultComboBoxModel<String>) view.getOrderFields().getModel();
		model3.removeAllElements();
		DefaultComboBoxModel<String> model4 = (DefaultComboBoxModel<String>) view.getOrdDetToDelete().getModel();
		model4.removeAllElements();
		for (String item : warehouseAdministration.getAllTheProductFields()) {
		    model.addElement(item);
		}
		for (String item : clientAdministration.getAllTheCustomerFields()) {
		    model2.addElement(item);
		}
		for (String item : odp.getAllTheOrderDetailFields()) {
		    model3.addElement(item);
		}
		for (String item : odp.getAllOrderDet()) {
		    model4.addElement(item);
		}
		view.getFieldsToEdit().setModel(model);
		view.getFieldsCtoEdit().setModel(model2);
		view.getOrderFields().setModel(model3);
		view.getOrdDetToDelete().setModel(model4);
    } 
    public void createBills(){
    	view.getBtnBills().addActionListener(new ActionListener(){
    		public void actionPerformed(ActionEvent e) {
    			try{
    				orderProcessing.makeBill();
				}catch(Exception e2){
					view.err(e2.getLocalizedMessage(),"Bills erros");
					e2.printStackTrace();
				}
    		} 
    	});
    } 
	public View getView() {
		return view;
	}
}
