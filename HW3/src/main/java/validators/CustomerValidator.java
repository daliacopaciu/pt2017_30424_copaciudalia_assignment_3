package validators;
import model.Customer;
/**
 * validates a customer's input data given by the user
 * @author Dali
 *
 */
public class CustomerValidator implements Validator<Customer> {
/**
 * verifies if the email contains an "@" and if the budget is a positive integer
 * @param Customer t
 */
	public void validate(Customer t) {
		if(!t.getEmail().contains("@") || t.getEmail().contains("/*^#!-=()%~`")){
			throw new IllegalArgumentException("Write the complete email: example@yahoo.com");
		}
		if(t.getBudget()<=0){
			throw new IllegalArgumentException("The customer must have some money in order to buy something");
		}
		
	}

}
