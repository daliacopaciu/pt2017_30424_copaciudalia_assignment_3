package validators;

import java.util.ArrayList;
import java.util.List;
import businessLogic.ClientAdministration;
import businessLogic.OrderProcessing;
import businessLogic.WarehouseAdministration;
import model.Order;
import model.OrderDetails;
import model.Product;
/**
 * validates an order detail's input data given by the user
 * @author Dali
 *
 */
public class OrderDetailValidator implements Validator<OrderDetails> {

	private WarehouseAdministration wA = new WarehouseAdministration();
	private OrderProcessing op=new OrderProcessing();
	private ClientAdministration clientAdministration = new ClientAdministration();
	/**
	 * verifies if the quantity> existing stock, if the quantity is a positive integer, if the product id and customer id give exist
	 */
	public void validate(OrderDetails o) {
		Product product = wA.getProductById(o.getProductID());
		Order order = op.findById(o.getOrderID());
		
		if(o.getQuantity() > product.getStock()){
			throw new IllegalArgumentException("The order is too big for the existing stock!");
		} 
		if(o.getQuantity() <= 0){
			throw new IllegalArgumentException("The quantity must be at least 1!");
		}  
    	List<Integer>id_customer=new ArrayList<Integer>();
    	List<Integer>id_prod=new ArrayList<Integer>();
    	id_prod=wA.getProductIds();
    	id_customer = clientAdministration.getAllTheCustomersIds();
    	if(!id_prod.contains(o.getProductID()))
    		throw new IllegalArgumentException("This product does not exist!");
		if(!id_customer.contains(order.getCustomerID()))
			throw new IllegalArgumentException("This customer does not exist!");
	}
}
