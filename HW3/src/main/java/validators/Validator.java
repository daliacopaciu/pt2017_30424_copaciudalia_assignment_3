package validators;
/**
 * interface for validators to implemet
 * @author Dali
 *
 * @param <T>
 */
public interface Validator<T> {
	public void validate(T t);
}
