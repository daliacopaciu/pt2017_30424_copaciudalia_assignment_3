package validators;

import model.Product;
/**
 * validates a product's input data given by the user
 * @author Dali
 *
 */
public class ProductValidator implements Validator<Product>  {
/**
 * verifies if the prive, weight and stock are positive integers
 */
	public void validate(Product product) {

		if (product.getPrice()<=0) {
			throw new IllegalArgumentException("The price must be a positive value!");
		}
		
		if (product.getWeight()<=0) {
			throw new IllegalArgumentException("The weight must be a positive value!");
		}
		
		if (product.getStock()<=0) {
			throw new IllegalArgumentException("The stock must be a positive value!");
		}
		

	}
	
}
