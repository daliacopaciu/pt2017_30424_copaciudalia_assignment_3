package businessLogic;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import dao.CustomerDAO;
import model.Customer;
/**
 * Gets the methods from CustomerDAO and has other auxiliary methods
 * @author Dali
 *
 */
public class ClientAdministration {
/**
 * 
 * @return all existing cusomers
 */
	public List<Customer> getAllTheCustomers(){
		return CustomerDAO.getAllCustomers();
	}
	/**
	 * 
	 * @param customerId
	 * @return all customers with id given as parameter
	 */
	public Customer findById(int customerId) {
		return CustomerDAO.findById(customerId);
	} 
	/**
	 * inserts in cutomer table customer given as parameter
	 * @param customer
	 */
	public void insertCustomer(Customer customer){
		CustomerDAO.insert(customer);
	}
	/**
	 * deletes in cutomer table customer whose id is given as parameter
	 * @param customerId
	 */
	public void deleteCustomer(int customerId){
		CustomerDAO.delete(customerId);
	}
	/**
	 * fills the customer table with its fields and then rows with data (all existing customers)
	 * @return
	 */
	public DefaultTableModel fillCustomer() {
		DefaultTableModel dtm = new DefaultTableModel();
		dtm.addColumn("id");
		dtm.addColumn("name");
		dtm.addColumn("budget");
		dtm.addColumn("address");
		dtm.addColumn("email");
		for (Customer c : CustomerDAO.getAllCustomers())
			dtm.addRow(new Object[] { c.getId(), c.getName(), c.getBudget(), c.getAddress(), c.getEmail() });
		return dtm;
	}
	/**
	 * 
	 * @return names and ids of all existing customers
	 */
	public List<String> getCustomersIdsAndNames(){
		List<String> result = new ArrayList<String>();
		for(Customer customer: getAllTheCustomers()){
			result.add(customer.getId() + " " + customer.getName());
		}
		return result;
	}
	/**
	 * 
	 * @param customerId
	 * @param field
	 * @param value
	 * @return editted customer's(whose id is given as parameter) fiels with value
	 */
	public Customer edit(int customerId,String field,String value){
		return CustomerDAO.edit(customerId, field, value);
	}
	/**
	 * edits the customer's (found by its id) field with value 
	 * @param customerId
	 * @param field
	 * @param value
	 * @return editted customer
	 */
	public Customer obtainedCustomer(int customerId, String field, String value){
		Customer customer = findById(customerId);
		if(field.equals("name")){
			customer.setName(value);
		}
		if(field.equals("budget")){
			customer.setBudget((Integer.parseInt(value)));
		}
		if(field.equals("address")){
			customer.setName(value);
		}
		if(field.equals("email")){
			customer.setName(value);
		}
		return customer;
	}
	/**
	 * 
	 * @return the fields of the customers except for the id field
	 */
	public List<String> getAllTheCustomerFields(){
		List<String> result = new ArrayList<String>();
		result.add("name");
		result.add("budget");
		result.add("address");
		result.add("email");
		return result;
	}
	/**
	 * 
	 * @return ids of all existing customers
	 */
	public List<Integer> getAllTheCustomersIds(){
		List<Integer> allTheCustomerIds = new ArrayList<Integer>();
		for(Customer customer: getAllTheCustomers()){
			allTheCustomerIds.add(customer.getId());
		}
		return allTheCustomerIds;
	}

}
