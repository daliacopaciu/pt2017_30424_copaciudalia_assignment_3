package businessLogic;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import java.io.*;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;

import dao.CustomerDAO;
import dao.OrderDAO;
import dao.OrderDetailDAO;
import dao.ProductDAO;
import model.Customer;
import model.Order;
import model.Product;
import model.OrderDetails;
/**
 * Gets the methods from OrderDAO and has other auxiliary methods
 * @author Dali
 *
 */
public class OrderProcessing {
	/**
	 * 
	 * @return all the existing orders
	 */
	public List<Order> getAllTheOrders(){
		return OrderDAO.getAllOrders();
	}
	/**
	 * 
	 * @param customerID
	 * @return all orders from a customer whose if is given as parameter
	 */
	public List<Order> findByCustomer(int customerID){
		return OrderDAO.findBy("customerid",customerID);
	}
	/**
	 * inserts in order table the order given as parameter
	 * @param order
	 */
	public void insertOrder(Order order){
		OrderDAO.insert(order);
	}
	/**
	 * deletes an order whose id is given as parameter
	 * @param orderId
	 */
	public void deleteOrder(int orderId){
		OrderDAO.delete(orderId);
	}
	/**
	 * updates in table order the order's(given by orderId) field with the value
	 * @param orderId
	 * @param field
	 * @param value
	 * @return editted order
	 */
	public Order edit(int orderId,String field,String value) {
		return OrderDAO.edit(orderId, field, value);
	}
	
    /**
     * fills the order table with its fields and then rows with data (all existing orders)
     * @return
     */
	public DefaultTableModel fillOrder() {
		DefaultTableModel dtm = new DefaultTableModel();
		dtm.addColumn("id");
		dtm.addColumn("customerID");
		dtm.addColumn("totalPrice");
		for (Order c : OrderDAO.getAllOrders())
			dtm.addRow(new Object[] { c.getId(), c.getCustomerID(),c.getTotalPrice() });
		return dtm;
	}
	/**
	 * 
	 * @return all customers's ids and names that have at least one order
	 */
	public List<String> getCustomersIdsAndNames(){
		List<String> result = new ArrayList<String>();
		for(Order order: OrderDAO.getAllOrders()){
			Customer c=CustomerDAO.findById(order.getCustomerID());
			result.add(order.getId() + " " + c.getName());
		}
		return result;
	}
	/**
	 * 
	 * @return all customers's names that have at least one order
	 */
	public List<Customer>getCustomersWithOrders(){
		List<Customer> result = new ArrayList<Customer>();
		for(Order order: OrderDAO.getAllOrders()){
			Customer c=CustomerDAO.findById(order.getCustomerID());
			result.add(c);
		}
		return result;
	}
	/**
	 * 
	 * @param by
	 * @param value
	 * @return all orders's fields give by the String "by" with value
	 */
	public List<Order> findBy(String by,int value) { 
		return OrderDAO.findBy(by,value);
	}
	/**
	 * 
	 * @param orderId
	 * @return the order whose id is given as parameter
	 */
	public Order findById(int orderId) { 
		return OrderDAO.findBy("id",orderId).get(0);
	}
	/**
	 * edits the order's (found by its id) field with value 
	 * @param orderId
	 * @param field
	 * @param value
	 * @return
	 */
	public Order obtainedOrder(int orderId, String field, String value){
		Order order = findById(orderId);
		if(field.equals("customerID")){
			order.setCustomerID(Integer.parseInt(value));
		}
		return order;
	}
	/**
	 * generates a bill(PDF file) for each customer that has at least an order (even if the order has no oder details)
	 */
	public void makeBill(){
		int ordnr=0,total=0,ord_price=0;
		String path="Bills/";
		File file=new File(path);
		file.mkdirs();
		FileOutputStream bill=null;
		for(Customer customer:getCustomersWithOrders()){       //get all existing customers
			total=0;
			ordnr=0;
			try {
				bill = new FileOutputStream(path+"bill"+customer.getName()+".pdf");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			PdfWriter writer=new PdfWriter(bill);
			PdfDocument pdf=new PdfDocument(writer);
			Document doc=new Document(pdf);
			doc.add(new Paragraph("Customer: "+customer.getName()+""));
			for(Order o: findBy("customerid",customer.getId())){
				ordnr++;
				doc.add(new Paragraph("Order "+ordnr+":"));
				ord_price=0;
				for(OrderDetails orddet:OrderDetailDAO.findBy("orderid",o.getId())){
					Product product=ProductDAO.findById(orddet.getProductID());
					doc.add(new Paragraph(product.getName()+" x "+orddet.getQuantity()+" => "+product.getPrice()*orddet.getQuantity()));
				    ord_price+=product.getPrice()*orddet.getQuantity();
				}
				doc.add(new Paragraph("Total order "+ordnr+":  "+ ord_price));
				total+=ord_price;
			}
			doc.add(new Paragraph("Total: "+total+""));
			doc.close(); 
		}
		
		
	} 
	
}
