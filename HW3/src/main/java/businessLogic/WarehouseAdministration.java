package businessLogic;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import dao.ProductDAO;
import model.Product;
import model.ProductCategories;
/**
 * Gets the methods from ProductDAO and has other auxiliary methods
 * @author Dali
 *
 */
public class WarehouseAdministration {
	/**
	 * inserts a product in the product table
	 * @param product
	 */
	public void insertProduct(Product product){
		ProductDAO.insert(product);
	}
	/**
	 * deletes from product table a product whose id is given as parameter
	 * @param productId
	 */
	public void deleteProduct(int productId){
		ProductDAO.delete(productId);
	}
	/**
	 * edits a product's(given by id) field with value
	 * @param productId
	 * @param field
	 * @param value
	 * @return editted product
	 */
	public Product editProduct(int productId, String field, String value){
		return ProductDAO.edit(productId, field, value);
	}
	/**
	 * 
	 * @return all editable fields of the product table
	 */
	public List<String> getAllTheProductFields(){
		List<String> result = new ArrayList<String>();
		result.add("name");
		result.add("price");
		result.add("category");
		result.add("weight");
		result.add("stock");
		return result;
	}
	/**
	 * fills the product table with its fields and then rows with data (products searched by a field whose value is given as a parameter)
	 * @param by
	 * @param value
	 * @return
	 */
	public DefaultTableModel showProductBy(String by,String value) {
		DefaultTableModel dtm = new DefaultTableModel();
		dtm.addColumn("id");
		dtm.addColumn("name");
		dtm.addColumn("category");
		dtm.addColumn("price");
		dtm.addColumn("weight");
		dtm.addColumn("stock");
		for (Product p : ProductDAO.findBy(by,value))
			dtm.addRow(new Object[] { p.getId(), p.getName(), p.getCategory(), p.getPrice(), p.getWeight(),p.getStock()});
		return dtm;
	}
	/**
	 * searches for all products whose field has the value given as parameter
	 * @param by
	 * @param value
	 * @return products satisfying the conditions
	 */
	public List<Product> findBy(String by,String value){
		return ProductDAO.findBy(by,value);
    } 
	/**
	 * 
	 * @return all existing products ids and names as strings
	 */
	public List<String> getProductIdsAndNames(){
		List<Product> allProducts = ProductDAO.findBy("all", "");
		List<String> resultList = new ArrayList<String>();
		for(Product product: allProducts){
			resultList.add(product.getId() + " " + product.getName());
		}
		return resultList;
	}
	/**
	 * 
	 * @return all existing products ids as integer
	 */
	public List<Integer> getProductIds(){
		List<Product> allProducts = ProductDAO.findBy("all", "");
		List<Integer> resultList = new ArrayList<Integer>();
		for(Product product: allProducts){
			resultList.add(product.getId());
		}
		return resultList;
	}
	/**
	 * 
	 * @param productId
	 * @return the product with id given as parameter
	 */
	public Product getProductById(int productId){
		return ProductDAO.findById(productId);
	} 
	/**
	 * edits the product's (found by its id) field with value 
	 * @param productId
	 * @param field
	 * @param value
	 * @return editted product
	 */
	public Product obtainedProduct(int productId, String field, String value){
		Product product = getProductById(productId);
		if(field.equals("name")){
			product.setName(value);
		}
		if(field.equals("price")){
			product.setPrice(Integer.parseInt(value));
		}
		if(field.equals("category")){
			product.setCategory(ProductCategories.fromStringToProductCategory(value));
		}
		if(field.equals("weight")){
			product.setWeight(Integer.parseInt(value));
		}
		if(field.equals("stock")){
			product.setStock(Integer.parseInt(value));
		}
		return product;
	}


}
