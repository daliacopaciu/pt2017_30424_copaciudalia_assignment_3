package businessLogic;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import dao.CustomerDAO;
import dao.OrderDAO;
import dao.OrderDetailDAO;
import dao.ProductDAO;
import model.Customer;
import model.Order;
import model.OrderDetails;
import model.Product;
/**
 * Gets the methods from OrderDetailDAO and has other auxiliary methods
 * @author Dali
 *
 */
public class OrderDetailsProcessing {
/**
 * 
 * @return all existing order details
 */
	public List<OrderDetails> getAllTheOrders(){
		return OrderDetailDAO.getAllOrders();
	}
	/**
	 * 
	 * @param orderId
	 * @return all order details whose order id is given as parameter
	 */
	public List<OrderDetails> findByOrderId(int orderId) {
		return OrderDetailDAO.findBy("orderid",orderId);
	}
	/**
	 * 
	 * @param by
	 * @param Id
	 * @return the order detail whose fields is given by the string "by" and the value of the field is id
	 */
	public OrderDetails findBy(String by,int Id) {
		return OrderDetailDAO.findBy(by,Id).get(0);
	}
	/**
	 * inserts in orderdetail table the order detail given as parameter
	 * @param order
	 */
	public void insert(OrderDetails order) {
		OrderDetails upd=processOrder(order);
		OrderDetailDAO.insert(upd);
	}
	/**
	 * uses the delete method from OrderDetailDAO and updates the customers's budget and product's stock
	 * @param id
	 * @param total
	 */
	public void delete(int id,boolean total) {
		if(!total){
		OrderDetails orderdet=OrderDetailDAO.findBy("id",id).get(0);   //the order detail to delete
		Order order=OrderDAO.findBy("id",orderdet.getOrderID()).get(0);  //the order to whom belongs this order detail
		Product product = ProductDAO.findById(orderdet.getProductID());    //the product in the order detail
		Customer customer = CustomerDAO.findById(order.getCustomerID());   //the customer in the order
		product.setStock(product.getStock()+orderdet.getQuantity());
		customer.setBudget(customer.getBudget()+orderdet.getQuantity()*product.getPrice());
		order.setTotalPrice(order.getTotalPrice()-orderdet.getQuantity()*product.getPrice());
		ProductDAO.update(product.getId(), product.getStock()+orderdet.getQuantity());    //the stock is incremented with the canceled quantity
		CustomerDAO.update(customer.getName(), customer.getBudget()+orderdet.getQuantity()*product.getPrice());
		OrderDAO.update(order.getId(),order.getTotalPrice()-orderdet.getQuantity()*product.getPrice() );
		}
		
		OrderDetailDAO.delete(id,total);
		
	}
	/**
	 * update in table orderdetails the order detail's(given by id) field with value
	 * @param Id
	 * @param field
	 * @param value
	 * @return editted order detail
	 */
	public OrderDetails edit(int Id,String field,String value) {
		return OrderDetailDAO.edit(Id, field, value);
	}
	/**
	 * decreases the customer's budget and the product's stock after an order detail is added
	 * @param order
	 * @return the editted order detail
	 */
	public OrderDetails processOrder(OrderDetails order){  
		Product product = ProductDAO.findById(order.getProductID());
		Order ord=OrderDAO.findBy("id",order.getOrderID()).get(0);   //the order to whom belongs this order detail
		Customer customer = CustomerDAO.findById(ord.getCustomerID());
		if(order.getQuantity() * product.getPrice()>customer.getBudget()) 
			throw new IllegalArgumentException("This customer does not have enough money to pay for this order!");
		int price=order.getQuantity() * product.getPrice();
		ord.setTotalPrice(ord.getTotalPrice()+price);
		customer.setBudget(customer.getBudget()-price);
		product.setStock(product.getStock()-order.getQuantity());
		ProductDAO.update(product.getId(), product.getStock()-order.getQuantity());
		CustomerDAO.update(customer.getName(),customer.getBudget()-price);
		OrderDAO.update(ord.getId(), ord.getTotalPrice());

		return order;
	}
	/**
	 * 
	 * @return the fields that can be editted from orderdetails table
	 */
	public List<String> getAllTheOrderDetailFields(){   //fields to edit
		List<String> result = new ArrayList<String>();
		result.add("productID");
		result.add("quantity");
		return result;
	}
	/**
	 * fills the orderdetail table with its fields and then rows with data (all existing order details)
	 * @return
	 */
	public DefaultTableModel fillOrderDetail() {
		DefaultTableModel dtm = new DefaultTableModel();
		dtm.addColumn("id");
		dtm.addColumn("orderID");
		dtm.addColumn("productID");
		dtm.addColumn("quantity");
		for (OrderDetails c : OrderDetailDAO.getAllOrders())
			dtm.addRow(new Object[] { c.getId(), c.getOrderID(),c.getProductID(),c.getQuantity() });
		return dtm;
	}
	/**
	 * 
	 * @return all the existing orderdetails's ids as strings
	 */
	public List<String> getAllOrderDet(){
		List<String> result = new ArrayList<String>();
		for (OrderDetails c : OrderDetailDAO.getAllOrders()){
			result.add(c.getId()+"");
		}
		return result;
	}
	/**
	 * edits the order detail's (found by its id) field with value 
	 * @param orderId
	 * @param field
	 * @param value
	 * @return
	 */
	public OrderDetails obtainedOrderDet(int orderId, String field, String value){
		OrderDetails orderdet = findBy("id",orderId);
		if(field.equals("productID")){
			orderdet.setProductID(Integer.parseInt(value));
		}
		else if(field.equals("quantityID")){
			orderdet.setQuantity(Integer.parseInt(value));
		}
		return orderdet;
	}
}
