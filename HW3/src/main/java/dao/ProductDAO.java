package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import dao.connection.ConnectionFactory;
import model.Product;
import model.ProductCategories;
/**
 * Data Access Object for Product
 * @author Dali
 *
 */
public class ProductDAO {
	protected static final Logger LOGGER = Logger.getLogger(ProductDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO product (name, price, category, weight, stock)"+ " VALUES (?,?,?,?,?)";
	private final static String findStatementStringByPrice = "SELECT * FROM product where price <= ?";
	private final static String findStatementStringById = "SELECT * FROM product where id = ?";
    private final static String findStatementStringByCategory = "SELECT * FROM product where category = ?";
    private final static String findStatementStringByName = "SELECT * FROM product where name = ?";
	private final static String getAllTheProducts = "SELECT * FROM product";
	private final static String deleteProduct = "DELETE FROM product WHERE id = ?";
	private final static String updateProductPrice = "UPDATE product SET stock = ? WHERE id = ?";
/**
 * searches the products by a given field with the specified value
 * @param by
 * @param value
 * @return products that satisfy the conditions
 */
	public static List<Product> findBy(String by,String value) {   
		List<Product> toReturn = new ArrayList<Product>();
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			if(by.equals("price")){
			    findStatement = dbConnection.prepareStatement(findStatementStringByPrice);
			    findStatement.setInt(1, Integer.parseInt(value));
			}
			else if(by.equals("category")){
				findStatement = dbConnection.prepareStatement(findStatementStringByCategory);
				findStatement.setString(1, value);
			}
			else if(by.equals("name")){               //if by="name"
				findStatement = dbConnection.prepareStatement(findStatementStringByName);
				findStatement.setString(1, value);
			}
			else if(by.equals("id")){
				findStatement = dbConnection.prepareStatement(findStatementStringById);
				findStatement.setInt(1, Integer.parseInt(value));
			}
			else findStatement = dbConnection.prepareStatement(getAllTheProducts);  //if by="all" => return all products
			rs = findStatement.executeQuery();
			try {
				while(rs.next()){
				String name = rs.getString("name");
				String category = rs.getString("category");
				ProductCategories productCategory = ProductCategories.fromStringToProductCategory(category);
				int price = rs.getInt("price");
				int weight = rs.getInt("weight");
				int stock = rs.getInt("stock");
				int id=rs.getInt("id");	
				toReturn.add(new Product(id,name, price, productCategory, weight, stock));}
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING,"productDAO:findBy " + e.getMessage());
			} finally {
				ConnectionFactory.close(rs);
				ConnectionFactory.close(findStatement);
				ConnectionFactory.close(dbConnection);
			}
		} catch (SQLException e) {
			System.out.println("Wrong here\n");
			e.printStackTrace();
		}
		return toReturn;
	} 
	/**
	 * finds a product by id
	 * @param productId
	 * @return product
	 */
	public static Product findById(int productId) {   
		List<Product> toReturn = new ArrayList<Product>();
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementStringById);
			findStatement.setInt(1, productId);
			rs = findStatement.executeQuery();
			try {
				while(rs.next()){
				String name = rs.getString("name");
				String category = rs.getString("category");
				ProductCategories productCategory = ProductCategories.fromStringToProductCategory(category);
				int price = rs.getInt("price");
				int weight = rs.getInt("weight");
				int stock = rs.getInt("stock");	
				toReturn.add(new Product(productId, name, price, productCategory, weight, stock));}
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING,"productDAO:findByPrice " + e.getMessage());
			} finally {
				ConnectionFactory.close(rs);
				ConnectionFactory.close(findStatement);
				ConnectionFactory.close(dbConnection);
			}
		} catch (SQLException e) {
			System.out.println("Wrong here\n");
			e.printStackTrace();
		}
		return toReturn.get(0);
	} 
	/**
	 * inserts a product in the table
	 * @param product
	 */
	public static void insert(Product product) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement insertStatement = null;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, product.getName());
			insertStatement.setInt(2, product.getPrice());
			insertStatement.setString(3, product.getCategory().toString());
			insertStatement.setInt(4, product.getWeight());
			insertStatement.setInt(5, product.getStock());
			insertStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	/**
	 * deletes a product given by id from the table
	 * @param productId
	 */
	public static void delete(int productId) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement deleteStatement = null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteProduct, Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, productId);
			deleteStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	/**
	 * updates a product's price with the value
	 * @param productID
	 * @param value
	 */
	public static void update(int productID, int value) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		try {
			updateStatement = dbConnection.prepareStatement(updateProductPrice, Statement.RETURN_GENERATED_KEYS);
			updateStatement.setInt(1, value);
			updateStatement.setInt(2, productID);
			updateStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO:update " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	/**
	 * edits a product's(given by id) field with value
	 * @param productId
	 * @param field
	 * @param value
	 * @return editted product
	 */
	public static Product edit(int productId,String field,String value) {
		Connection dbConnection = ConnectionFactory.getConnection();
		String editStatement = "UPDATE product SET "+field+"=? WHERE id=?";
		PreparedStatement updateStatement = null;
		try {
			updateStatement = dbConnection.prepareStatement(editStatement, Statement.RETURN_GENERATED_KEYS);
			if(field.equals("name") || field.equals("category")){
				updateStatement.setString(1, value);
			}
			if(field.equals("price") || field.equals("weight") || field.equals("stock")){
				updateStatement.setInt(1, Integer.parseInt(value));
			}
			updateStatement.setInt(2, productId);
			updateStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO:update " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
		return null;
	}
}
