package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import dao.connection.ConnectionFactory;
import model.Customer;
/**
 * Data Access Object for Customer
 * @author Dali
 *
 */
public class CustomerDAO {

	protected static final Logger LOGGER = Logger.getLogger(CustomerDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO customer (name, budget, address,email)"
			+ " VALUES (?,?,?,?)";
	private final static String findStatementStringById = "SELECT * FROM customer where id = ?";
	private final static String getAllTheCustomers = "SELECT * FROM customer";
	private final static String deleteCustomer = "DELETE FROM customer WHERE id = ?";
	private final static String updateCustomerBudget = "UPDATE customer SET budget = ? WHERE name = ?";
	/**
	 * finds customer by customer id given as parameter
	 * @param customerId
	 * @return customer
	 */
	public static Customer findById(int customerId) {   //finds products with price <= product Price
		Customer toReturn=new Customer();

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementStringById);
			findStatement.setInt(1, customerId);
			rs = findStatement.executeQuery();
			
			try {
				while(rs.next()){
				String name = rs.getString("name");
				int budget = rs.getInt("budget");
				String address=rs.getString("address");
				String email=rs.getString("email");
					
				toReturn=new Customer(customerId, name, budget,address,email);}
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING,"productDAO:findByPrice " + e.getMessage());
			} finally {
				ConnectionFactory.close(rs);
				ConnectionFactory.close(findStatement);
				ConnectionFactory.close(dbConnection);
			}
			
		} catch (SQLException e) {
			System.out.println("Wrong here\n");
			e.printStackTrace();
		}
		return toReturn;
	}
	/**
	 *searches for all the exising customers in the DB
	 * @return list of customers
	 */
	public static List<Customer> getAllCustomers() {   //finds all customers with the name name
		List<Customer> toReturn = new ArrayList<Customer>();
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(getAllTheCustomers);
			rs = findStatement.executeQuery();
			try {
				while(rs.next()){
					String name=rs.getString("name");
					String address = rs.getString("address");
					int budget = rs.getInt("budget");
					String email=rs.getString("email");
					int id=rs.getInt("id");
						
					toReturn.add(new Customer(id,name,budget,address,email));}
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING,"productDAO:findByPrice " + e.getMessage());
			} finally {
				ConnectionFactory.close(rs);
				ConnectionFactory.close(findStatement);
				ConnectionFactory.close(dbConnection);
			}
		} catch (SQLException e) {
			System.out.println("Wrong here\n");
			e.printStackTrace();
		}
		return toReturn;
	}
	/**
	 * inserts a customer given as parameter
	 * @param customer
	 */
	public static void insert(Customer customer){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement insertStatement = null;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, customer.getName());
			insertStatement.setInt(2, customer.getBudget());
			insertStatement.setString(3, customer.getAddress());
			insertStatement.setString(4, customer.getEmail());
			insertStatement.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "CustomerDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	/**
	 * deletes a customer whose id is given as parameter
	 * @param customerId
	 */
	public static void delete(int customerId){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteCustomer, Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, customerId);
			deleteStatement.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	/**
	 * edits a customer's (found by customerID) field (given as string) with the value (given as String)
	 * @param customerId
	 * @param field
	 * @param value
	 * @return editted customer
	 */
	public static Customer edit(int customerId,String field,String value) {
		Connection dbConnection = ConnectionFactory.getConnection();
		String editStatement = "UPDATE customer SET "+field+"=? WHERE id=?";
		PreparedStatement updateStatement = null;
		try {
			updateStatement = dbConnection.prepareStatement(editStatement, Statement.RETURN_GENERATED_KEYS);
			if(field.equals("name") || field.equals("address") || field.equals("email")){
				updateStatement.setString(1, value);
			}
			if(field.equals("budget")){
				updateStatement.setInt(1, Integer.parseInt(value));
			}
			updateStatement.setInt(2, customerId);
			updateStatement.executeUpdate();
			Customer customer = findById(customerId);
			return customer;
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO:update " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
		return null;
	}
	/**
	 * updates customers's budget
	 * @param customerName
	 * @param value
	 */
	public static void update(String customerName, int value) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		try {
			updateStatement = dbConnection.prepareStatement(updateCustomerBudget, Statement.RETURN_GENERATED_KEYS);
			updateStatement.setInt(1, value);
			updateStatement.setString(2, customerName);
			updateStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "CustomerDAO:update " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
}
