package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import dao.connection.ConnectionFactory;
import model.Order;
/**
 * Data Access Object for Order
 * @author Dali
 *
 */
public class OrderDAO {
	protected static final Logger LOGGER = Logger.getLogger(OrderDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO hw3.order (customerID)"
			+ " VALUES (?)";
	private final static String findStatementStringById = "SELECT * FROM hw3.order where id = ?";
	private final static String findStatementStringByCustomerID= "SELECT * FROM hw3.order where customerID = ?";
	private final static String getAllTheOrders = "SELECT * FROM hw3.order";
	private final static String deleteOrder = "DELETE FROM hw3.order WHERE id = ?";
	private final static String updateOrderPrice = "UPDATE hw3.order SET totalPrice = ? where id =?";
/**
 * finds orders by a field whose value is given
 * @param by
 * @param value
 * @return list of orders satisfying the condition
 */
	public static List<Order> findBy(String by,int value) {  //if by="id" => search an order by id, if by="customerid" => search an order by customer id
		List<Order> toReturn = new ArrayList<Order>();

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			if(by.equals("customerid"))
			    findStatement = dbConnection.prepareStatement(findStatementStringByCustomerID);
			else   //if by="id"
				findStatement = dbConnection.prepareStatement(findStatementStringById);
			findStatement.setInt(1, value);
			rs = findStatement.executeQuery();
			
			try {
				while(rs.next()){
				int id=rs.getInt("id");
				int customerID=rs.getInt("customerID");
				int totalPrice = rs.getInt("totalPrice");
				toReturn.add(new Order(id,customerID,totalPrice));}
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING,"productDAO:findByPrice " + e.getMessage());
			} finally {
				ConnectionFactory.close(rs);
				ConnectionFactory.close(findStatement);
				ConnectionFactory.close(dbConnection);			}
			
		} catch (SQLException e) {
			System.out.println("Wrong here\n");
			e.printStackTrace();
		}
		
		return toReturn;
	}
	/**
	 * updates the order's (given by order id) price
	 * @param orderId
	 * @param newTotalPrice
	 */
	public static void update(int orderId, int newTotalPrice) {
		Connection dbConnection = ConnectionFactory.getConnection();
		
		PreparedStatement updateStatement = null;
		try {
			updateStatement = dbConnection.prepareStatement(updateOrderPrice, Statement.RETURN_GENERATED_KEYS);
			updateStatement.setInt(1, newTotalPrice);
			updateStatement.setInt(2, orderId);
			updateStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderDAO:update " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	/**
	 * inserts an order
	 * @param order
	 */
	public static void insert(Order order) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement insertStatement = null;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, order.getCustomerID());
			insertStatement.executeUpdate();

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OderDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	/**
	 * searches for all existing orders
	 * @return list of orders
	 */
	public static List<Order> getAllOrders(){
		List<Order> resultList = new ArrayList<Order>();
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(getAllTheOrders);
			rs = findStatement.executeQuery();
			while(rs.next()){
			int id = rs.getInt("id");
			int customerID = rs.getInt("customerID");
			int totalPrice = rs.getInt("totalPrice");

			Order order = new Order(id, customerID,totalPrice);
			order.setTotalPrice(totalPrice);
			resultList.add(order);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"OrderDAO:getAllOrders " + e.getMessage());
		}  finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return resultList;
	}
	/**
	 * deletes an order given by order id
	 * @param orderId
	 */
	public static void delete(int orderId) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement deleteStatement = null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteOrder, Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, orderId);
			deleteStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderDAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	/**
	 * edits an order's (given by order id) field with the value
	 * @param orderId
	 * @param field
	 * @param value
	 * @return editted order
	 */
	public static Order edit(int orderId,String field,String value) {
		Connection dbConnection = ConnectionFactory.getConnection();
		String editStatement = "UPDATE hw3.order SET "+field+"=? WHERE id=?";
		PreparedStatement updateStatement = null;
		try {
			updateStatement = dbConnection.prepareStatement(editStatement, Statement.RETURN_GENERATED_KEYS);
			if(field.equals("customerID")){
				updateStatement.setInt(1, Integer.parseInt(value));
			}
			
			updateStatement.setInt(2, orderId);
			updateStatement.executeUpdate();
			Order order = findBy("id",orderId).get(0);
			return order;
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderDAO:edit " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
		return null;
	}
}
