package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import dao.connection.ConnectionFactory;
import model.OrderDetails;
/**
 * Data Access Object for Order detail
 * @author Dali
 *
 */
public class OrderDetailDAO {
	protected static final Logger LOGGER = Logger.getLogger(OrderDetailDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO orderdetails (orderID,productID, quantity)"
			+ " VALUES (?,?,?)";
	private final static String findStatementStringByOrderId = "SELECT * FROM orderdetails where orderID = ?";
	private final static String findStatementStringById = "SELECT * FROM orderdetails where id = ?";
	private final static String getAllTheOrders = "SELECT * FROM orderdetails";
	private final static String deleteOrder = "DELETE FROM orderdetails WHERE id = ?";
	private final static String deleteTotalOrder = "DELETE FROM orderdetails WHERE orderID = ?";
	/**
	 * searches for all the existing order details
	 * @return list of orders details
	 */
	public static List<OrderDetails> getAllOrders(){
		List<OrderDetails> resultList = new ArrayList<OrderDetails>();
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(getAllTheOrders);
			rs = findStatement.executeQuery();
			while(rs.next()){
			int id = rs.getInt("id");
			int orderID = rs.getInt("orderID");
			int productID = rs.getInt("productID");
			int quantity = rs.getInt("quantity");

			OrderDetails order = new OrderDetails(id, orderID,productID,quantity);
			resultList.add(order);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"orderDetailDAO: get all orders " + e.getMessage());
		}  finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return resultList;
	}
	/**
	 * finds the order details from an order given by its order id
	 * @param by
	 * @param orderId
	 * @return list of order details
	 */
	public static List<OrderDetails> findBy(String by,int orderId) {   //finds order details by order id

		List<OrderDetails> toReturn=new ArrayList<OrderDetails>();

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			if(by.equals("orderid"))
			findStatement = dbConnection.prepareStatement(findStatementStringByOrderId);
			else findStatement = dbConnection.prepareStatement(findStatementStringById);
			findStatement.setInt(1, orderId);
			rs = findStatement.executeQuery();
			
			try {
				while(rs.next()){
				int productID = rs.getInt("productID");
				int quantity = rs.getInt("quantity");
				int id=rs.getInt("id");
					
				toReturn.add(new OrderDetails(id,orderId,productID,quantity));}
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING,"orderDetailDAO:findByOrderID " + e.getMessage());
			} finally {
				ConnectionFactory.close(rs);
				ConnectionFactory.close(findStatement);
				ConnectionFactory.close(dbConnection);
			}
			
		} catch (SQLException e) {
			System.out.println("Wrong here\n");
			e.printStackTrace();
		}
		
		return toReturn;
}
	/**
	 * inserts an order detail
	 * @param order
	 */
	public static void insert(OrderDetails order) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement insertStatement = null;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, order.getOrderID());
			insertStatement.setInt(2, order.getProductID());
			insertStatement.setInt(3, order.getQuantity());
			insertStatement.executeUpdate();

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "orderDetailDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
   /**
    * delets an order detail or more order details
    * @if total=true => deletes all the order details from an order give by its is
    * @if total=false => deletes only the order detail with the specified id
    * @param id
    * @param total
    */
	public static void delete(int id,boolean total) {    //if total=true => delete all order details from an order
		Connection dbConnection = ConnectionFactory.getConnection();    //if ok=false => delete order detail by id

		PreparedStatement deleteStatement = null;
		try {
			if(!total)
			deleteStatement = dbConnection.prepareStatement(deleteOrder, Statement.RETURN_GENERATED_KEYS);
			else deleteStatement = dbConnection.prepareStatement(deleteTotalOrder, Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, id);
			deleteStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderDetailDAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	/**
	 * edits an order detail's (given by id) specified field with the value
	 * @param Id
	 * @param field
	 * @param value
	 * @return editted order detail
	 */
	public static OrderDetails edit(int Id,String field,String value) {
		Connection dbConnection = ConnectionFactory.getConnection();
		String editStatement = "UPDATE orderdetails SET "+field+"=? WHERE id=?";
		PreparedStatement updateStatement = null;
		try {
			updateStatement = dbConnection.prepareStatement(editStatement, Statement.RETURN_GENERATED_KEYS);
			updateStatement.setInt(1, Integer.parseInt(value));
			
			updateStatement.setInt(2, Id);
			updateStatement.executeUpdate();
			OrderDetails order = findBy("id",Id).get(0);
			return order;
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "orderDetailDAO: edit " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
		return null;
	}
}
