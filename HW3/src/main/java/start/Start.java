package start;

import model.ProductCategories;
import presentation.Controller;
import presentation.View;
/**
 * class from which the application simulation can start
 * @author Dali
 *
 */
public class Start {
	/**
	 * instantiate a view and a controller
	 * @param args
	 */
	public static void main(String[] args) {
		View view = new View(ProductCategories.categoriesArray());
		Controller controller = new Controller(view);
		controller.getView().frame.setVisible(true);
		
	}

}
